package ru.kupriyanov.springcourse.models;

import ru.kupriyanov.springcourse.interfarce.IMusic;

public final class ClassicalMusic implements IMusic {

    // фабричный метод
    public static ClassicalMusic getClassicalMusic() {
        System.out.println("Using fabric to create ClassicalMusic.");
        return new ClassicalMusic();
    }

    private ClassicalMusic() {
    }

    public void Init() {
        System.out.println("Initialization of ClassicalMusic.");
    }

    public void Destroy() {
        System.out.println("Destruction of ClassicalMusic.");
    }

    @Override
    public String getSong() {
        return "classical song";
    }

}
