package ru.kupriyanov.springcourse.models;

import ru.kupriyanov.springcourse.interfarce.IMusic;

public final class RockMusic implements IMusic {

    @Override
    public String getSong() {
        return "rock song";
    }

    public void Init() {
        System.out.println("Initialization of RockMusic.");
    }

    public void Destroy() {
        System.out.println("Destruction of RockMusic.");
    }

}
